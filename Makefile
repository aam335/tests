CC = gcc
CFLAGS = -g -std=c99 -D_GNU_SOURCE -D__USE_GNU -funsigned-char -Wno-pointer-sign -Wall -DDEVELOP 
#-DUSE_OPENSSL_PRE_1_1_API=1 
USERLIBS = $(SYSLIBS) -lsrtp2 -lev -lmbedtls -lmbedx509  -lmbedcrypto #-lpcap
DEPLIBS =
LDLIBS = $(USERLIBS) 

# make && install mbedtls
# wget https://tls.mbed.org/download/mbedtls-2.16.4-apache.tgz
# make && make install DESTDIR=~/.local

LD_LIBRARY_PATH= -L ~/.local/lib
INCLUDE_PATH=-I ~/.local/include

SRC = $(shell find ./ -name "*.c" | grep -v tests| grep -v obsolete)
OBJ = $(patsubst ./%.c,./build/%.o,$(SRC))

.PHONY: all

all: dtls_client

strip: dtls_client
	strip mediabridge

clean:
	rm -rf dtls_client
	rm -f ./build/*

$(OBJ): ./build/%.o: %.c
	@mkdir -p $(dir $@)
	$(CC) -c $(CFLAGS) $(CFLAGSD) $(INCLUDE_PATH) -MMD $< -o $@

dtls_client:  $(OBJ)
	$(CC) $(CFLAGS) $(CFLAGSD) $(LDFLAGS) $(LD_LIBRARY_PATH)  $(OBJ) $(LDLIBS) -o $@

#genkeys: 
#	openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 -keyout keys-ssl/key.pem -out keys-ssl/cert.pem \
#	-subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com"
#
#keys-ssl/key.pem: genkeys
#
#keys-ssl/cert.pem: genkeys


-include $(patsubst ./%.c,./build/%.d,$(SRC))

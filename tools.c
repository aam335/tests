#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/epoll.h>
#include <time.h>
#include <dirent.h>
#include <sys/time.h>
#include <stdint.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <ctype.h>
//#include <sys/param.h>
#include <dirent.h>
#include <pthread.h>
#include "tools.h"

/** 
 *      @ver 1.0 2015/12/28
 */


//#define MAX_LOG_LEN 1000
int log_fd = -1;
int debug = 0;
char mylog_buffer[MAX_LOG_LEN];

pthread_mutex_t log_mutex = PTHREAD_MUTEX_INITIALIZER;

void sleep_ms(int milliseconds) // cross-platform sleep function
{
#ifdef WIN32
    Sleep(milliseconds);
#elif _POSIX_C_SOURCE >= 199309L
    struct timespec ts;
    ts.tv_sec = milliseconds / 1000;
    ts.tv_nsec = (milliseconds % 1000) * 1000000;
    nanosleep(&ts, NULL);
#else
    usleep(milliseconds * 1000);
#endif
}

typedef struct stat Stat;

/** Перераспределяю память на 1/8 от max_size или на size_add (или что больше)
 *  
 * @param ptr указатель на распределенную память
 * @param old_size - размер уже распеределенной памяти
 * @param size_add - сколько требуется добавить точно
 * @param max_size - максимальный лимит на объем выделяемой памяти
 * @return <0 если превышение size_max или памяти в системе не хватает
 */

int do_realloc(void **ptr, size_t old_size, size_t size_add, size_t max_size) {
    ssize_t new_size = ((old_size + size_add) / (max_size / 8) + 1) * (max_size / 8);
    if (new_size > max_size) {
        return -1;
    }
    void *new_ptr = realloc(*ptr, new_size);
    if (!new_ptr) {
        return -1;
    }
    *ptr = new_ptr;
    return new_size;
}
#undef get16bits
#if (defined(__GNUC__) && defined(__i386__)) || defined(__WATCOMC__) \
  || defined(_MSC_VER) || defined (__BORLANDC__) || defined (__TURBOC__)
#define get16bits(d) (*((const uint16_t *) (d)))
#endif

#if !defined (get16bits)
#define get16bits(d) ((((uint32_t)(((const uint8_t *)(d))[1])) << 8)\
                       +(uint32_t)(((const uint8_t *)(d))[0]) )
#endif

uint32_t FastHashU32(const char * data, int len) {
    uint32_t hash = len, tmp;
    int rem;

    if (len <= 0 || data == NULL) return 0;

    rem = len & 3;
    len >>= 2;

    /* Main loop */
    for (; len > 0; len--) {
        hash += get16bits(data);
        tmp = (get16bits(data + 2) << 11) ^ hash;
        hash = (hash << 16) ^ tmp;
        data += 2 * sizeof (uint16_t);
        hash += hash >> 11;
    }

    /* Handle end cases */
    switch (rem) {
        case 3: hash += get16bits(data);
            hash ^= hash << 16;
            hash ^= ((signed char) data[sizeof (uint16_t)]) << 18;
            hash += hash >> 11;
            break;
        case 2: hash += get16bits(data);
            hash ^= hash << 11;
            hash += hash >> 17;
            break;
        case 1: hash += (signed char) *data;
            hash ^= hash << 10;
            hash += hash >> 1;
    }

    /* Force "avalanching" of final 127 bits */
    hash ^= hash << 3;
    hash += hash >> 5;
    hash ^= hash << 4;
    hash += hash >> 17;
    hash ^= hash << 25;
    hash += hash >> 6;

    return hash;
}

static int do_mkdir(const char *path, mode_t mode) {
    Stat st;
    int status = 0;

    if (stat(path, &st) != 0) {
        /* Directory does not exist. EEXIST for race condition */
        if (mkdir(path, mode) != 0 && errno != EEXIST)
            status = -1;
    } else if (!S_ISDIR(st.st_mode)) {
        errno = ENOTDIR;
        status = -1;
    }

    return (status);
}

/**
 * Удаляет нэймспейсы из XML
 * @param str
 * @param str_len
 * @return 
 */
char *xml_remove_namespaces(char *str, size_t str_len) {
    char *s, *start_pos;
    int mode = 0;
    char *s1 = (char *) calloc(1, str_len + 1);
    char *save_s1 = s1;
    s = str;
    while ((s1 - save_s1 < str_len)&&*s) {
        if (mode == 0) { // ищем '<'
            if (*s == '<') {
                *s1 = *s;
                s++;
                s1++;
                if (*s == '/') {
                    *s1 = *s;
                    s++;
                    s1++;
                }
                mode = 1;
                start_pos = s1;
                continue;
            }
        } else
            if (mode == 1) {
            if (*s == ':') {
                s1 = start_pos;
                s++;
                continue;
            } else if (!isalnum(*s)) { // любой символ кроме имени 
                mode = 0;
            }
        }
        *s1 = *s;
        s++;
        s1++;
    }
    *s1 = '\0';
    //    printf("{%s}{%s}\n", str, save_s1);
    return save_s1;
}

/**
 * добавляем байты в файл. если превышает maxsize - ротирую,
 * если нужно - откоываю новый
 * @param bin_fd дескриптор
 * @param filename имя 
 * @param str данные
 * @param n длина данных
 * @param maxsize максимальный размер файла
 * @return 
 */

int append_bin(int bin_fd, char *filename, char *str, int n, int max_size) {
    //    char tmp[MAXNAMLEN];
    char tmp[PATH_MAX];
    if (bin_fd > 0) { // если лог открыт и размер файла больше максимально разрешенного, ротация
        struct stat buf;
        fstat(bin_fd, &buf);
        if (buf.st_size > max_size) {
            //            strcpy(tmp, filename);
            //            strcat(tmp, ".prev");
            snprintf(tmp, PATH_MAX, "%s.prev", filename);
            close(bin_fd);
            unlink(tmp);
            rename(filename, tmp);
            bin_fd = -2;
        }
    }

    if (bin_fd < 0) {
        bin_fd = open(filename, O_APPEND | O_WRONLY | O_CREAT, 0644);
    }

    if (bin_fd >= 0) {
        write(bin_fd, str, n);
        fsync(bin_fd);
    }
    return bin_fd;
}

/**
 ** mkpath - ensure all directories in path exist
 ** Algorithm takes the pessimistic view and works top-down to ensure
 ** each directory in path exists, rather than optimistically creating
 ** the last element and working backwards.
 */
int mkpath(const char *path, mode_t mode) {
    char *pp;
    char *sp;
    int status;
    char *copypath = strdup(path);

    status = 0;
    pp = copypath;
    while (status == 0 && (sp = strchr(pp, '/')) != 0) //'/' - path from root
    {
        if (sp != pp) {
            /* Neither root nor double slash in path */
            *sp = '\0';
            status = do_mkdir(copypath, mode);
            *sp = '/';
        }
        pp = sp + 1;
    }
    if (status == 0)
        status = do_mkdir(path, mode);
    free(copypath);
    return (status);
}

int path_open(char *path, char *file_name, mode_t mode) {
    int res;
    char tmpstr[PATH_MAX]; //))))

    snprintf(tmpstr, PATH_MAX, "%s/%s", path, file_name);
    res = open(tmpstr, O_WRONLY | O_CREAT | O_TRUNC, mode);

    if (res < 0) { // если ошибка - делаем путь
        res = mkpath(path, 0775);
        if (res >= 0) {
            res = open(tmpstr, O_WRONLY | O_CREAT, mode);
        }
        return res;
    }
    return res;
}

static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
    'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
    'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
    'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
    'w', 'x', 'y', 'z', '0', '1', '2', '3',
    '4', '5', '6', '7', '8', '9', '+', '/'};
static int mod_table[] = {0, 2, 1};

char *base64_encode(uint8_t *data,
        size_t input_length,
        size_t *output_length) {
    int i, j;
    //    uint32_t octet_a, octet_b, octet_c, triple;

    *output_length = 4 * ((input_length + 2) / 3);

    char *encoded_data = (char*)malloc(*output_length + 1);
    if (encoded_data == NULL) return NULL;

    for (i = 0, j = 0; i < input_length;) {

        uint32_t octet_a = i < input_length ? (unsigned char) data[i++] : 0;
        uint32_t octet_b = i < input_length ? (unsigned char) data[i++] : 0;
        uint32_t octet_c = i < input_length ? (unsigned char) data[i++] : 0;

        uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

        encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    }

    for (i = 0; i < mod_table[input_length % 3]; i++)
        encoded_data[*output_length - 1 - i] = '=';
    encoded_data[*output_length] = '\0';
    return encoded_data;
}


static const char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
// static const int BASE64_INPUT_SIZE = 57;

int8_t isbase64(char c) {
    return c && strchr(table, c) != NULL;
}

//inline 

char value(char c) {
    const char *p = strchr(table, c);
    if (p) {
        return p - table;
    } else {
        return 0;
    }
}

/**
 * Декодирование Base64
 * @param dest
 * @param src
 * @param srclen
 * @return 
 */


int UnBase64(uint8_t *dest, const uint8_t *src, int srclen) {
    *dest = 0;
    if (*src == 0) {
        return 0;
    }
    uint8_t *p = dest;
    do {

        char a = value(src[0]);
        char b = value(src[1]);
        char c = value(src[2]);
        char d = value(src[3]);
        *p++ = (a << 2) | (b >> 4);
        *p++ = (b << 4) | (c >> 2);
        *p++ = (c << 6) | d;
        if (!isbase64(src[1])) {
            p -= 2;
            break;
        } else if (!isbase64(src[2])) {
            p -= 2;
            break;
        } else if (!isbase64(src[3])) {
            p--;
            break;
        }
        src += 4;
        while (*src && (*src == 13 || *src == 10)) src++;
    } while (srclen -= 4);
    *p = 0;
    return p - dest;
}

/*
unsigned char *base64_decode(const char *data,
        size_t input_length,
        size_t *output_length) {

    if (decoding_table == NULL) build_decoding_table();

    if (input_length % 4 != 0) return NULL;

 *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;

    unsigned char *decoded_data = malloc(*output_length);
    if (decoded_data == NULL) return NULL;

    for ( i = 0, j = 0; i < input_length;) {

        uint32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        uint32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];

        uint32_t triple = (sextet_a << 3 * 6)
                + (sextet_b << 2 * 6)
                + (sextet_c << 1 * 6)
                + (sextet_d << 0 * 6);

        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }

    return decoded_data;
}

void build_decoding_table() {

    decoding_table = malloc(256);

    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}

void base64_cleanup() {
    free(decoding_table);
}
 */
#if 0

char *debug_epoll_event(char *events, struct epoll_event ev) {

    sprintf(events, "fd:%d, ev.events:", ev.data.fd);

    if (ev.events & EPOLLIN)
        strcat(events, " EPOLLIN ");
    if (ev.events & EPOLLOUT)
        strcat(events, " EPOLLOUT ");
    if (ev.events & EPOLLET)
        strcat(events, " EPOLLET ");
    if (ev.events & EPOLLPRI)
        strcat(events, " EPOLLPRI ");
    if (ev.events & EPOLLRDNORM)
        strcat(events, " EPOLLRDNORM ");
    if (ev.events & EPOLLRDBAND)
        strcat(events, " EPOLLRDBAND ");
    if (ev.events & EPOLLWRNORM)
        strcat(events, " EPOLLRDNORM ");
    if (ev.events & EPOLLWRBAND)
        strcat(events, " EPOLLWRBAND ");
    if (ev.events & EPOLLMSG)
        strcat(events, " EPOLLMSG ");
    if (ev.events & EPOLLERR)
        strcat(events, " EPOLLERR ");
    if (ev.events & EPOLLHUP)
        strcat(events, " EPOLLHUP ");
    if (ev.events & EPOLLONESHOT)
        strcat(events, " EPOLLONESHOT ");

    return events;
}
#endif
/**
 * логгинг в файл с таймстампом
 * 
 * @param str строка для записи
 * @return 
 */

static char *LOG_PATH = NULL,
        *LOG_PATH_OLD = NULL;
static int MAX_LOG_SIZE = 100000;

void mylog_init(char *lp, char *lpo, int maxlog_size) {
    if (LOG_PATH) free(LOG_PATH);
    if (LOG_PATH_OLD) free(LOG_PATH_OLD);
    //    pthread_mutex_init(&log_mutex,NULL);
    if (lp == NULL) { //default init
        char tmp[MAX_LOG_LEN], tmp1[MAX_LOG_LEN], *s;
        int f = open("/proc/self/cmdline", O_RDONLY);
        if (f >= 0) {
            read(f, tmp, MAX_LOG_LEN);
            close(f);
            s = strrchr(tmp, '/');
            if (!s) s = tmp;
        } else {
            s = "logfile";
        }
        snprintf(tmp1, MAX_LOG_LEN, "/tmp/%s.log", s);
        LOG_PATH = strdup(tmp1);
        snprintf(tmp1, MAX_LOG_LEN, "/tmp/%s.old.log", s);
        LOG_PATH_OLD = strdup(tmp1);
        unlink(LOG_PATH_OLD);
        rename(LOG_PATH, LOG_PATH_OLD);
    } else {
        LOG_PATH = strdup(lp);
        LOG_PATH_OLD = strdup(lpo);
        MAX_LOG_SIZE = maxlog_size;
    }
}

int mylog(char *str) {
    char log_buff[MAX_LOG_LEN];
    if (!LOG_PATH) mylog_init(NULL, NULL, 0);
    if (log_fd > 0) { // если лог открыт и размер файла больше максимально разрешенного, ротация
        struct stat buf;
        fstat(log_fd, &buf);
        if (buf.st_size > MAX_LOG_SIZE) {
            close(log_fd);
            unlink(LOG_PATH_OLD);
            rename(LOG_PATH, LOG_PATH_OLD);
            log_fd = -2;
        }
    }
    if (log_fd < 0) {
        log_fd = open(LOG_PATH, O_APPEND | O_WRONLY | O_CREAT, 0644);
    }

    if (log_fd >= 0) {
        time_t rawtime;
        struct tm *info;
        int time_len = 0, n;

        rawtime = time(NULL);

        info = localtime(&rawtime);

        time_len += strftime(log_buff, MAX_LOG_LEN, "%FT%T ", info);

        n = strlen(str);

        if (time_len + n > MAX_LOG_LEN) { // обрезаем если длина строки превышает размет буфера
            n = MAX_LOG_LEN - time_len - 2;
        }

        strncpy(log_buff + time_len, str, n);
        n += time_len;

        if (log_buff[n - 1] == '\n') {
            n--;
        }


        log_buff[n] = '\n';
        log_buff[n + 1] = '\0';
        n++;

        //        pthread_mutex_lock(&log_mutex);
        write(log_fd, log_buff, n);
        if (debug > 3) {
            fsync(log_fd);
        }

        if (debug) {
            printf("%s", log_buff);
        }
        //        pthread_mutex_unlock(&log_mutex);

    }
    return 1;
}

int64_t microtime() {
    struct timeval tv;
    long long ms = 0LL;

    gettimeofday(&tv, NULL);

    ms = tv.tv_sec;
    ms = ms * 1000 + tv.tv_usec / 1000;

    return ms;
}

/**
 * Преобразует последовательность байт в читаемый дамп
 * @param str куда складывать
 * @param maxlen макс. длина
 * @param buff данные
 * @param bufflen длина данных
 */


void bin2hexstr(char *str, int maxlen, uint8_t *buff, int bufflen) {
    char *hex = "0123456789ABCDEF";
    int pos = 0;
    int i = 0;
    // "01 "....
    while (i + 3 + 1 < maxlen && pos < bufflen) {
        if ((pos & 0xf) == 0) i += snprintf(str + i, maxlen - i, "\n%04x\t", pos);
        str[i++] = hex[buff[pos] >> 4];
        str[i++] = hex[0xf & buff[pos]];
        str[i++] = ' ';
        pos++;
    }
    str[i] = '\0';
}
// что и куда

int dump_mac(void *mac_, char *str) {
    uint8_t *mac = (uint8_t*)mac_;
    snprintf((char*) str, 20, "%02x:%02x:%02x:%02x:%02x:%02x", *mac, *(mac + 1), *(mac + 2), *(mac + 3), *(mac + 4), *(mac + 5));
    return 1;
}
// на вход - 6 байт мака, выход строка
static char mac0[20];
char* dump_mac0(void *mac_) {
    uint8_t *mac = (uint8_t*)mac_;
    snprintf((char*) mac0, 20, "%02x:%02x:%02x:%02x:%02x:%02x", *mac, *(mac + 1), *(mac + 2), *(mac + 3), *(mac + 4), *(mac + 5));
    return mac0;
}

char *parse_mac(const char *str, unsigned char *const mac) {
    int len = -1;
    if (sscanf(str, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx%n", mac + 0, mac + 1, mac + 2, mac + 3, mac + 4, mac + 5, &len) < 6 || len < 11)
        return NULL; /* Not a valid MAC address */

    /* Return a pointer to the first unparsed character */
    return (char*) str + len;
}

/**
 * 
 * @param hostname char*
 * @param portNumber int
 * @return
 */

int getSocket(char *hostname, int portNumber, struct sockaddr_in *addr0) {

    //    struct sockaddr_in addr;
    //    struct hostent *he;
    //    int sock;
    //    if (hostname == NULL) {
    //        //        perror ( "host is undefined!" );
    //        return -1;
    //    }
    //
    //    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    //        //      perror ( "socket" );
    //        return -2;
    //    }
    //
    //    make_socket_non_blocking(sock);
    //    bzero((char *) &addr, sizeof ( struct sockaddr_in));
    //
    //    if ((he = gethostbyname(hostname)) == NULL) {
    //        //    perror ( "No such host" );
    //        return -3;
    //    }
    //    
    //    bcopy(he->h_addr, &addr.sin_addr, he->h_length);
    //    addr.sin_family = AF_INET;
    //    addr.sin_port = htons(portNumber);
    //    bcopy(&addr, addr0, sizeof (struct sockaddr_in));
    //    if (connect(sock, (struct sockaddr *) &addr, sizeof ( addr)) < 0) {
    //        //        perror ( "connect" );
    //        if (errno != EINPROGRESS) {
    //            return -4;
    //        }
    //    }
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int sfd, s; //, j;
    //  size_t len;
    //    ssize_t nread;
    char strPort[10];
    snprintf(strPort, 10, "%i", portNumber);
    //    char buf[BUF_SIZE];

    /* Obtain address(es) matching host/port */

    memset(&hints, 0, sizeof (struct addrinfo));
    hints.ai_family = AF_UNSPEC; /* Allow IPv4 or IPv6 */
    hints.ai_socktype = SOCK_STREAM; /* Datagram socket */
    hints.ai_flags = 0;
    hints.ai_protocol = 0; /* Any protocol */

    s = getaddrinfo(hostname, strPort, &hints, &result);
    if (s != 0) {
        dlogprintf("getaddrinfo: %s\n", gai_strerror(s));
        return -1;
    }

    /* getaddrinfo() returns a list of address structures.
       Try each address until we successfully connect(2).
       If socket(2) (or connect(2)) fails, we (close the socket
       and) try the next address. */

    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sfd = socket(rp->ai_family, rp->ai_socktype,
                rp->ai_protocol);
        if (sfd == -1)
            continue;
        if (addr0)
            memcpy(addr0, rp->ai_addr, sizeof (struct sockaddr_in));

        make_socket_non_blocking(sfd);

        if (connect(sfd, rp->ai_addr, rp->ai_addrlen) != -1)
            break; /* Success */
        else if (errno == EINPROGRESS)
            break;

        close(sfd);
    }

    freeaddrinfo(result); /* No longer needed */

    if (rp == NULL) { /* No address succeeded */
        dlogprintf("Could not connect %s %i (%s)\n", hostname, portNumber, inet_ntoa(((struct sockaddr_in*) rp->ai_addr)->sin_addr));
        return -1;
    }
    return sfd;
}

/**
 * 
 * @param host
 * @param port
 * @param ai_socktype
 * @return 
 */

int create_non_blocked_listener_addr(int port, int ai_socktype, in_addr_t in_addr) {
    int s, sfd;
    int opt = 1;
    struct sockaddr_in addr;
    switch (ai_socktype) {
        case SOCK_STREAM:
            sfd = socket(AF_INET, ai_socktype, IPPROTO_TCP);
            break;
        case SOCK_DGRAM:
            sfd = socket(AF_INET, ai_socktype, IPPROTO_UDP);
            break;
        default:
            dlogprintf("illegal ai_socktype %i", ai_socktype);
            return -1;
    }
    if (sfd == -1 || (setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof opt) < 0)) {
        dlogprintf("socket err: %s\r\n", strerror(errno));
        return -1;
    }
    memset(&addr, 0, sizeof (addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = in_addr; //INADDR_ANY;

    s = bind(sfd, (struct sockaddr*) &addr, sizeof (addr));
    if (s) {
        dlogprintf("bind err: %s\r\n", strerror(errno));
        return -1;
    }

    s = make_socket_non_blocking(sfd);
    if (s == -1) {
        dlogprintf("Error: make_socket_non_blocking\n");
        return -1;
    }

    // если TCP - настраиваем листенер

    if (ai_socktype == SOCK_STREAM) {
        s = listen(sfd, SOMAXCONN);
        if (s == -1) {
            dlogprintf("Error: listen\n");
            return -1;
        }
    }
    return sfd;
}

int make_socket_non_blocking(int sfd) {
    int flags;

    /* If they have O_NONBLOCK, use the Posix way to do it */
#if defined(O_NONBLOCK)
    /* Fixme: O_NONBLOCK is defined but broken on SunOS 4.1.x and AIX 3.2.5. */
    if (-1 == (flags = fcntl(sfd, F_GETFL, 0)))
        flags = 0;
    return fcntl(sfd, F_SETFL, flags | O_NONBLOCK);
#else
    /* Otherwise, use the old way of doing it */
    flags = 1;
    return ioctl(fd, FIOBIO, &flags);
#endif
}

/**
 * Вычитывает буффер без блокировки
 * 
 * @param fd
 * @param in_buff
 * @param in_buff_size
 * @param ret - 
 * @return 
 */
int recvnb(int fd, char *in_buff, int in_buff_size, int *ret) {
    int in_size = 0;
    //    int close_session = 0;
    int count;
    while (1) {
        count = recv(fd, in_buff + in_size, in_buff_size - in_size, 0);
        if (count == -1) {
            /* If errno == EAGAIN, that means we have read all
               data. So go back to the main loop. */
            if (errno == EAGAIN) {
                count = 1;
            } else {
                dlogprintf("read error %s fd %i", strerror(errno), fd);
            }
            break;
        } else if (count == 0) {
            /* End of file. The remote has closed the
               connection. */
            DLP7("EOF fd %i", fd);
            break;
        }
        // проверяем на максимальный размер в 512 байт
        // БОЛЬШЕ НЕ НУЖНО
        in_size += count;
        if (in_size == in_buff_size) {
            break;
        }
    }
    if (ret) *ret = count;
    return count >= 0 ? in_size : count;
}

int sockaddr_cmp(struct sockaddr *x, struct sockaddr *y) {
#define CMP(a, b) if (a != b) return a < b ? -1 : 1

    CMP(x->sa_family, y->sa_family);

    if (x->sa_family == AF_UNIX) {
        struct sockaddr_un *xun = (struct sockaddr_un *) x, *yun = (struct sockaddr_un *) y;
        int r = strcmp(xun->sun_path, yun->sun_path);
        if (r != 0)
            return r;
    } else if (x->sa_family == AF_INET) {
        struct sockaddr_in *xin = (struct sockaddr_in*) x, *yin = (struct sockaddr_in*) y;
        CMP(ntohl(xin->sin_addr.s_addr), ntohl(yin->sin_addr.s_addr));
        CMP(ntohs(xin->sin_port), ntohs(yin->sin_port));
    } else if (x->sa_family == AF_INET6) {
        struct sockaddr_in6 *xin6 = (struct sockaddr_in6*) x, *yin6 = (struct sockaddr_in6*) y;
        int r = memcmp(xin6->sin6_addr.s6_addr, yin6->sin6_addr.s6_addr, sizeof (xin6->sin6_addr.s6_addr));
        if (r != 0)
            return r;
        CMP(ntohs(xin6->sin6_port), ntohs(yin6->sin6_port));
        CMP(xin6->sin6_flowinfo, yin6->sin6_flowinfo);
        CMP(xin6->sin6_scope_id, yin6->sin6_scope_id);
    } else {
        dlogprintf("unknown sa_family");
    }

#undef CMP
    return 0;
}

Uri_t *UriParce(char *uri) {
    if (!uri) return NULL;
    char *token = NULL, *token1;
    Uri_t *u = (Uri_t *)calloc(1, sizeof (Uri_t));
    char *_line = strdup(uri);
    char *lp = _line;
    int UriLen;

    u->_line = _line;
    // 1. Scheme
    token = strchr(lp, ':');
    if (token) {
        *token = '\0';
        u->Scheme = lp;
        lp = token + 1;
    } else {
        goto err;
    }
    // 2.  "//" && authority
    if (lp[0] == '/' && lp[1] == '/') {
        lp += 2;
        token1 = strchr(lp, '/');
        token = strchr(lp, '@');

        if (token && token1 > token) { // HAS LOGIN||LOGIN:PASS;
            *token = '\0';
            //            token1 = strchr(lp, ':');
            //            if (token1) { // Login:pass
            //                *token1 = '\0';
            //                u->Pass = token1 + 1;
            //            }
            u->UserInfo = lp;
            lp = token + 1;
        }
    } else {
        goto err;
    }
    // 3. host && path
    u->Host = lp;
    token = strchr(lp, ':'); // port
    token1 = strchr(lp, '/');
    if (token ) { //&& token < token1
        *token = '\0';
        u->Port = token + 1;
        u->PortInt = atoi(token + 1);
        lp = token + 1;
    }
    token = strchr(lp, '/');
    if (token) {
        token1 = strchr(token, '?');
        if (token1) {
            *token1 = '\0';
            u->Query = token1 + 1;
        }
        u->Path = strdup(token);
        *token = '\0';
    }
    UriLen = strlen(uri) + 10;
    u->Uri = (char*)calloc(1, UriLen);

    if (!strcmp(u->Scheme, "http") && !u->PortInt) {
        u->PortInt = 80;
    }

    snprintf(u->Uri, UriLen, "%s://%s:%i%s%s%s",
            u->Scheme, u->Host, u->PortInt, u->Path ? u->Path : "/", u->Query ? "?" : "", u->Query ? u->Query : "");


    //    free(_line);
    return u;
err:
    dlogprintf("Scheme error at %s", uri);
    free(_line);
    XFREE(u->Path);
    XFREE(u->Uri);
    free(u);
    return NULL;
}

void UriFree(Uri_t *u) {
    if (u != NULL) {
        XFREE(u->_line);
        XFREE(u->Path);
        XFREE(u->Uri);
        free(u);
    }
}

char *fileGetContents(char *filename) {
    int fd = open(filename, O_RDONLY);
    if (fd < 0)
        return NULL;
    struct stat st;
    if (fstat(fd, &st)) {
        close(fd);
        return NULL;
    }
    char *data = (char *)malloc(st.st_size + 1);
    int ret = read(fd, data, st.st_size);
    close(fd);
    if (ret != st.st_size) {
        free(data);
        return NULL;
    }
    data[st.st_size] = 0;
    return data;
}

void filePutContents(char *filename, void *data, int size) {
    int fd = open(filename, O_CREAT | O_WRONLY);
    if (fd < 0)
        return;
    write(fd, data, size);
    close(fd);
    return;
}

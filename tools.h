/* 
 * File:   tools.h
 * Author: andrey
 *
 * Created on 5 Сентябрь 2014 г., 8:26
 */

#ifndef TOOLS_H
#define TOOLS_H

#ifdef __cplusplus
extern "C" {
#endif
#include <sys/epoll.h>
#include <ev.h>
#include <linux/limits.h>
#include <netinet/in.h>
#include <stdio.h>
#include <signal.h>

#include <stdint.h>
#include <inttypes.h>

#ifdef USE_PTHREAD
#include <pthread.h>
#endif


#ifndef SOL_TCP
#define SOL_TCP 6  // socket options TCP level
#endif
#ifndef TCP_USER_TIMEOUT
#define TCP_USER_TIMEOUT 18  // how long for loss retry before timeout [ms]
#endif

    typedef enum HttpMethod_t {
        HTTP_GET, HTTP_PUT, HTTP_POST
    } HttpMethod_t;

    // int <0 = err, >0 = ReplySize, 0=size unknown
    // void * - cb data
    // char * = read buffer
    // ssize_t = bytes readed;
    // return = >0 ок <0 force close connection;

    typedef int (*HttpResultCB_t)(int, void*, char *, ssize_t);

    typedef struct Uri_t {
        char *_line;
        char *Scheme;
        char *UserInfo;
        char *Host;
        char *Port;
        int PortInt;
        char *Path;
        char *Query;
        char *Uri;
    } Uri_t;

    typedef struct HttpClient_t {
        //        char *Host;
        //        int Port;
        //        char *Url;
        char *Query;
        Uri_t *uri;
        HttpMethod_t Method;
        char *Body;
        ssize_t ContentLenght;
        HttpResultCB_t cb;
        void *data;
        int Fd;
        char *ReadBuffer;
        ssize_t ReadBufferSize;
        ssize_t ReadBufferRecvd;
        char *SentBuffer;
        ssize_t SentBufferSize;
        ssize_t SentBufferSent;
        uint8_t HeaderReceived;
        struct sockaddr_in Addr;
        EV_P;
        ev_io w;
        int FileFd;
    } HttpClient_t;

    typedef struct HttpGetFile_t {
        ssize_t ContentLenght;
        HttpResultCB_t cb;
        void *data;
        int Fd;
        uint8_t HeaderReceived;
        struct sockaddr_in Addr;
        EV_P;
        ev_io w;
        int FileFd;
        Uri_t *u;
        //        int Offset;
        //        int LastRet;
        char FileName[PATH_MAX];
    } HttpGetFile_t;

    // Free if not NULL and set to NULL
#define XFREE(x) if (x) { free(x); x=NULL; }
    void sleep_ms(int milliseconds); // cross-platform sleep function
    
    extern int do_realloc(void **ptr, size_t old_size, size_t size_add, size_t max_size)
    __THROW __attribute_warn_unused_result__;

    extern int mkpath(const char *path, mode_t mode);
    extern int path_open(char *path, char *file_name, mode_t mode);
    extern char *base64_encode(uint8_t *data, size_t input_length, size_t * output_length);
    extern char *debug_epoll_event(char *enents, struct epoll_event ev);
    extern int dump_mac(void *mac, char* str);
    char* dump_mac0(void *mac); // на вход - 6 байт мака, выход строка
    char *parse_mac(const char *str, unsigned char *const mac);

    extern void bin2hexstr(char *str, int maxlen, uint8_t *buff, int bufflen);
    extern int UnBase64(uint8_t *dest, const uint8_t *src, int srclen);
    extern uint32_t FastHashU32(const char *data, int len);

#define create_non_blocked_listener(port, ai_socktype) \
    create_non_blocked_listener_addr(port, ai_socktype, INADDR_ANY)

    int create_non_blocked_listener_addr(int port, int ai_socktype, in_addr_t addr);

    int getSocket(char *hostname, int portNumber, struct sockaddr_in *addr);
    int make_socket_non_blocking(int sfd);
    int sockaddr_cmp(struct sockaddr *x, struct sockaddr *y);
    int recvnb(int fd, char *in_buff, int in_buff_size, int *ret);

    char *fileGetContents(char *filename);
    void filePutContents(char *filename, void *data, int size);

    void UriFree(Uri_t *uri);
    Uri_t *UriParce(char *uri);

    int HttpGetFile(EV_P_ HttpGetFile_t *c, Uri_t *u, char *path, char *filename,
            HttpResultCB_t cb, void *cb_data);
    void HttpGetFileStop(EV_P_ HttpGetFile_t *c);
    /*
    #define LOG_PATH	"/tmp/socketserver.txt"
    #define LOG_PATH_OLD	"/tmp/socketserver.old.txt"
    #define MAX_LOG_SIZE	500000
     */

#define MAX_LOG_LEN     32000

    extern int mylog(char *str);
    extern int debug;
    extern char mylog_buffer[];
    int64_t microtime();
    extern pthread_mutex_t log_mutex;
    //#define dlogprintf(fmt, ...) {snprintf(mylog_buffer,MAX_LOG_LEN,fmt,##__VA_ARGS__);mylog(mylog_buffer);}
#ifndef USE_PTHREAD
#define dlogprintf(fmt, ...) {snprintf(mylog_buffer,MAX_LOG_LEN,"%s [%i]:" fmt,__FUNCTION__,__LINE__,##__VA_ARGS__);mylog(mylog_buffer);}
#else 
#define dlogprintf(fmt, ...) {pthread_mutex_lock(&log_mutex);snprintf(mylog_buffer,MAX_LOG_LEN,"%lu\t%s [%i]:" fmt,pthread_self(),__FUNCTION__,__LINE__,##__VA_ARGS__);mylog(mylog_buffer);pthread_mutex_unlock(&log_mutex);}
#endif
#define dperror(text) dlogprintf("Error on %s: %s", text,strerror(errno))

#define dlogprintf_start() {snprintf(mylog_buffer,MAX_LOG_LEN,">%s[%i]:", __FUNCTION__,__LINE__);}
#define dlogprintf_put(fmt, ...) {int i; i=strlen(mylog_buffer); snprintf(mylog_buffer+i,MAX_LOG_LEN-i, fmt,##__VA_ARGS__);}
#define dlogprintf_out() {mylog(mylog_buffer);}
#define dump_sa(x) dlogprintf("%s:%i (%i)",inet_ntoa(((struct sockaddr_in*)x)->sin_addr),ntohs(((struct sockaddr_in*)x)->sin_port),((struct sockaddr_in*)x)->sin_family)
#define LOG_IF_DEBUG(fmt, ...) {}
#define DLP1(fmt,...) {if(debug>=1){dlogprintf(fmt,##__VA_ARGS__);}}
#define DLP2(fmt,...) {if(debug>=2){dlogprintf(fmt,##__VA_ARGS__);}}
#define DLP3(fmt,...) {if(debug>=3){dlogprintf(fmt,##__VA_ARGS__);}}
#define DLP4(fmt,...) {if(debug>=4){dlogprintf(fmt,##__VA_ARGS__);}}
#define DLP5(fmt,...) {if(debug>=5){dlogprintf(fmt,##__VA_ARGS__);}}
#define DLP6(fmt,...) {if(debug>=6){dlogprintf(fmt,##__VA_ARGS__);}}
#define DLP7(fmt,...) {if(debug>=7){dlogprintf(fmt,##__VA_ARGS__);}}
#define DLP8(fmt,...) {if(debug>=7){dlogprintf(fmt,##__VA_ARGS__);}}

#if 0
#define dpAssert(e)       \
do {         \
        if (!(e)) {       \
            dlogprintf("ASSERT!");\
	}        \
} while (0)

#define AZ(foo)  do { dpAassert((foo) == 0); } while (0)
#define AN(foo)  do { dpAassert((foo) != 0); } while (0)
#endif


    // coRoutines
#define crBegin switch(crStaticVar) { case 0:
#define crReturn(x) do { crStaticVar=__LINE__; return x ; case __LINE__:; } while (0)
#define crFinish }


#ifndef     TAILQ_FOREACH_SAFE
#define TAILQ_FOREACH_SAFE(var, head, field, tvar)                      \
        for ((var) = TAILQ_FIRST((head));                               \
            (var) && ((tvar) = TAILQ_NEXT((var), field), 1);            \
            (var) = (tvar))
#endif


#ifdef __cplusplus
}
#endif

#endif /* TOOLS_H */

